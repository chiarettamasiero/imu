#!/usr/bin/python3 -u
## 
## usage: convert.py -i <inputfile> -o <outputfile>
##

import sys, getopt

EXIT_STATUS_NORMAL = 0
EXIT_STATUS_ERROR = 2

header = "time , dt , accx , accy , accz , magx , magy , magz , gyrox , gyroy , gyroz , lat , lon , h , fix"+"\n"

def usage(exitStatus):
	print ('convert.py -i <inputfile> -o <outputfile>')
	sys.exit(exitStatus)

def main(argv):

	inputfile = ''
	outputfile = ''
	try:
		opts, args = getopt.getopt(argv,"hi:o:")
		if not opts:
			usage(EXIT_STATUS_ERROR)
	except getopt.GetoptError:
		usage(EXIT_STATUS_ERROR)
	for opt, arg in opts:
		if opt == '-h':
			usage(EXIT_STATUS_NORMAL)
		elif opt in ("-i", "--ifile"):
			inputfile = arg
		elif opt in ("-o", "--ofile"):
			outputfile = arg


	fileNmea = open('nmea_'+outputfile.split(".csv")[0]+".nmea",'w+')
	file = open(outputfile, 'w+')
	file.write(header)

	with open(inputfile) as f:
		linecounter = 0
		toWrite=""
		time,dt,accx,accy,accz,magx,magy,magz,gyrox,gyroy,gyroz,lat,lon,h,fix = 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
		for line in f:
			if (linecounter == 1 or linecounter == 5):
				newline = line.split(":")[1].replace("\n","")
				gyrox = newline.split(",")[0]
				gyroy = newline.split(",")[1]
				gyroz = newline.split(",")[2]
			elif (linecounter == 2 or linecounter == 6):
				newline = line.split(":")[1].replace("\n","")
				accx = newline.split(",")[0]
				accy = newline.split(",")[1]
				accz = newline.split(",")[2]
			elif (linecounter == 3 or linecounter == 7):
				newline = line.split(":")[1].replace("\n","")
				magx = newline.split(",")[0]
				magy = newline.split(",")[1]
				magz = newline.split(",")[2]
			elif linecounter == 4:
				# is NMEA
				if line[0] == '$':
					fileNmea.write(line)
				nmeaHead = line.split(",")[0]
				if( nmeaHead == "$GNGGA" or nmeaHead ==  "$GNRMC" or nmeaHead == "$GNZDA"):
					time =  line.split(",")[1]
				else:
					continue
			if linecounter == 7:
				linecounter = 0
				toWrite = (str(time)+" , "+str(dt)+" , "+str(accx)+
    			" , "+str(accy)+" , "+str(accz)+" , "+str(magx)+
    			" , "+str(magy)+" , "+str(magz)+" , "+str(gyrox)+
    			" , "+str(gyroy)+" , "+str(gyroz)+" , "+str(lat)+
    			" , "+str(lon)+" , "+str(h)+" , "+str(fix)+"\n")
				file.write(toWrite)
				toWrite=""
			else:
				linecounter+=1



if __name__ == "__main__":
    main(sys.argv[1:])
